class BaseActionHandler(object):
    def __init__(self, library, player, config):
        """
        :param library: A library object
        :param jukky.players.mpc.MpcClient player:
        :param config: A configuration dictionary
        """
        self.library = library
        self.player = player
        self.config = config
