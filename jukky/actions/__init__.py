from .button import ButtonActionHandler
from .rfid import RFIDActionHandler
from .status import StatusActionHandler

ACTION_TYPE_BUTTON = 'button'
ACTION_TYPE_RFID = 'rfid'
ACTION_TYPE_STATUS = 'status'


def handle_action(action_type, value, library, player, config):
    action = None
    if action_type == ACTION_TYPE_BUTTON:
        action = ButtonActionHandler(library, player, config)
    elif action_type == ACTION_TYPE_RFID:
        action = RFIDActionHandler(library, player, config)
    elif action_type == ACTION_TYPE_STATUS:
        action = StatusActionHandler(library, player, config)
    if action:
        action.handle(value)
