import logging

from .helper import shutdown
from .base import BaseActionHandler

ACTION_SHUTDOWN = 'shutdown'
ACTION_PLAYPAUSE = 'playpause'
ACTION_NEXT = 'next'
ACTION_PREVIOUS = 'previous'
ACTION_VOL_UP = 'vol_up'
ACTION_VOL_DOWN = 'vol_down'
ACTION_LOW_BATTERY = 'low_battery'
CONFIGURATION_MAPPING = {}

logger = logging.getLogger('jukky')


def for_action(action):
    """Decorator to bind a ActionClass method to a specific action.

    :param action: The action to bind the method to
    """
    def wrapper(func):
        CONFIGURATION_MAPPING[action] = func
        def wraps(*args, **kwargs):
            func(args, kwargs)
        return wraps
    return wrapper


class ButtonActionHandler(BaseActionHandler):
    def handle(self, value):
        action = self._get_action_for_pin(value)
        if action:
            action_method = CONFIGURATION_MAPPING.get(action)
            if action_method:
                logger.info('Executing method "{}" for action "{}"'.format(action_method.__name__, action))
                action_method(self)
            else:
                logger.warning('No action available for {}'.format(action))
        else:
            logger.warning('No action configured for pin: {}'.format(value))

    def _get_action_for_pin(self, pin):
        """Get the configured action for the given pin."""
        if self.config.get('low_battery') == pin:
            return ACTION_LOW_BATTERY
        for key, conf_pin in self.config.get('interaction').items():
            if pin == conf_pin:
                return key
        return

    @for_action(ACTION_SHUTDOWN)
    def shutdown(self):
        shutdown()

    @for_action(ACTION_PLAYPAUSE)
    def playpause(self):
        self.player.switch_play_pause()

    @for_action(ACTION_NEXT)
    def next(self):
        self.player.next()

    @for_action(ACTION_PREVIOUS)
    def previous(self):
        self.player.previous()

    @for_action(ACTION_VOL_UP)
    def vol_up(self):
        max_volume = self.config.get('max_volume', 70)
        new_volume = self.player.volume + self.config.get('delta_volume', 5)
        if new_volume > max_volume:
            new_volume = max_volume
        self.player.volume = new_volume

    @for_action(ACTION_VOL_DOWN)
    def vol_down(self):
        self.player.volume -= self.config.get('delta_volume', 5)
