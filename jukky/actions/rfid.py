import logging

from .base import BaseActionHandler


logger = logging.getLogger('jukky')


class RFIDActionHandler(BaseActionHandler):
    """"""
    def handle(self, value):
        folder_to_play = self.library.find_directory_to_play(value)
        if folder_to_play:
            logger.info('Playing folder {}'.format(folder_to_play))
            self.player.play_directory(folder_to_play)
        else:
            logger.info('Paylist ID {} not yet configured.'.format(value))
