import logging

from .base import BaseActionHandler


logger = logging.getLogger('jukky')


class StatusActionHandler(BaseActionHandler):
    def handle(self, value):
        if value == 'inactive':
            logger.info('Inactivity detected. Shutdown now!')
        elif value == 'low_battery':
            logger.warning('Low battery! Shutdown now.')
        else:
            logger.warning('Unsupported status action: {}'.format(value))
            return
        #shutdown()
