import random

from .base import BaseProcess
from .helper import get_rfid_module


class RFIDReaderProcess(BaseProcess):
    def run(self):
        RFID = get_rfid_module()
        rdr = RFID()
        while True:
            rdr.wait_for_tag()
            (error, tag_type) = rdr.request()
            if not error:
                print("Tag detected")
                (error, uid) = rdr.anticoll()
                if not error:
                    print("UID: " + str(uid))
                    # Select Tag is required before Auth
                    if not rdr.select_tag(uid):
                        # Auth for block 10 (block 2 of sector 2) using default shipping key A
                        if not rdr.card_auth(rdr.auth_a, 10, [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF], uid):
                            error, content = rdr.read(10)
                            # This will print something like (False, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
                            print("Content of block 10: {}".format(content))
                            is_empty = self.is_key_empty(content)
                            if is_empty:
                                content = self.generate_random_content()
                                print("Generated: {}".format(content))
                                rdr.write(10, content)
                                self.write_file(self.generate_playlist_name(content))
                            # Always stop crypto1 when done working
                            rdr.stop_crypto()
                            self.con.send(self.generate_playlist_name(content))

    def is_key_empty(self, content_list):
        byte_list = list(set(content_list))
        return len(byte_list) == 1 and byte_list[0] == 0

    def generate_random_content(self):
        return [random.randrange(0, 255) for i in range(0, 16)]

    def generate_playlist_name(self, content_list):
        return '-'.join([str(i) for i in content_list])

    def write_file(self, name):
        print("Write file: {}".format(name))
