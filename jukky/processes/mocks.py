import os
import time

from mock import Mock


class GPIOMock(Mock):
    """
    A mock class to be able to test the application not only on a Raspberry Pi.

    The RPi.GPIO can only be imported and used on a Raspberry PI. To be able
    to test the application, a mock class is available and will be returned
    if the RPi.GPIO can not be imported.
    """
    def add_event_detect(self, pin, typing, callback=None, **kwargs):
        fake_pin = os.environ.get('JUKKY_FAKE_BUTTON', None)
        if fake_pin and pin == int(fake_pin):
            time.sleep(3)
            callback(pin)


class RFIDMock(Mock):
    fake_rfid_tag = os.environ.get('JUKKY_FAKE_RFID_TAG', None)

    def wait_for_tag(self):
        if self.fake_rfid_tag:
            time.sleep(2)
            # Just end method
            return
        # No fake tag, therefore run forever
        while True:
            pass

    def request(self):
        return (False, None)

    def anticoll(self):
        return (False, '76a5d76a5d765ad76a58d7')

    def select_tag(self, uid):
        return False

    def card_auth(self, *args, **kwargs):
        return False

    def read(self, block):
        ret = False, [int(s) for s in self.fake_rfid_tag.split('-')]
        self.fake_rfid_tag = None
        return ret
