import time
from datetime import datetime, timedelta

from .base import BaseProcess


class InactivityWatchProcess(BaseProcess):
    def run(self):
        while True:
            if self.application.player.is_playing():
                continue
            delta_last_active = datetime.now() - self.application.last_activity
            if delta_last_active > timedelta(minutes=self.config.get('inactivity_shutdown', 5)):
                self.con.send('inactive')
            # Check every minute
            time.sleep(60)
