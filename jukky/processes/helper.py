"""Some helper utilities for the processes."""


def get_gpio_module():
    """Try to import and return the RPi.GPIO module, otherwise return mock.

    The RPi.GPIO can only be imported and used on a Raspberry Pi. To be able
    to test the application, a mock class is available and will be returned
    if the RPi.GPIO can not be imported.
    """
    try:
        import RPi.GPIO as GPIO
    except RuntimeError:
        from .mocks import GPIOMock
        GPIO = GPIOMock()
    return GPIO


def get_rfid_module():
    """Try to import and return the pirc5222 RFID module, otherwise return a mock."""
    try:
        from pirc522 import RFID
    except (RuntimeError, ImportError):
        from .mocks import RFIDMock
        RFID = RFIDMock
    return RFID


def button_callback(process):
    """A generic callback for any GPIO channel events."""
    def wrapper(channel):
        process.callback(channel)
    return wrapper
