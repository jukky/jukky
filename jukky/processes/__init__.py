from .buttons import ButtonWatchProcess  # noqa
from .rfid import RFIDReaderProcess  # noqa
from .low_battery import LowBatteryWatchProcess  # noqa
from .inactivity_check import InactivityWatchProcess  # noqa
