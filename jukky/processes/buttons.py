import time

from .base import BaseGPIOProcess
from .helper import button_callback

ACTION_SHUTDOWN = 'shutdown'


class ButtonWatchProcess(BaseGPIOProcess):
    """Process to watch any button press."""
    gpio = None

    def run(self):
        self.setup_gpio()
        self.setup_pins()

        while True:
            time.sleep(3600)

    def setup_pins(self):
        for pin in self.config.get('interaction').values():
            # setup the pin to check the buttons - use the internal pull down resistor
            self.gpio.setup(pin, self.gpio.IN, pull_up_down=self.gpio.PUD_DOWN)
            self.gpio.add_event_detect(pin,
                                       self.gpio.RISING,
                                       callback=button_callback(self),
                                       bouncetime=300)

    def callback(self, channel):
        self.con.send(channel)
