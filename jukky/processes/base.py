from .helper import get_gpio_module


class BaseProcess(object):

    def __init__(self, connection, application):
        """
        :param multiprocessing.Connection connection: The connection
        :param Application application: The application object
        """
        self.con = connection
        self.config = application.config
        self.player = application.player
        self.application = application

    def run(self):
        raise NotImplementedError()


class BaseGPIOProcess(BaseProcess):
    def setup_gpio(self):
        self.gpio = get_gpio_module()
        self.gpio.setwarnings(False)
        self.gpio.setmode(self.gpio.BOARD)
