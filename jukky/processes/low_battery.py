import time

from actions.button import ACTION_LOW_BATTERY
from .helper import button_callback
from .base import BaseGPIOProcess


class LowBatteryWatchProcess(BaseGPIOProcess):
    def run(self):
        self.setup_gpio()
        shutdown_listening_pin = self.config.get(ACTION_LOW_BATTERY)
        if shutdown_listening_pin:
            self.gpio.setup(shutdown_listening_pin, self.gpio.IN)
            self.gpio.add_event_detect(shutdown_listening_pin,
                                       self.gpio.FALLING,
                                       callback=button_callback(self),
                                       bouncetime=300)
        while True:
            time.sleep(3600)

    def callback(self, channel):
        self.con.send('low_battery')
