import os
import re


class Library(object):

    def __init__(self, path):
        self.path = path

    def find_directory_to_play(self, playlist_id):
        """Find a folder containing a file named like the given playlist_id.

        :param playlist_id: The id to filter for
        :return: The first found directory or None
        """
        folder = None
        for root, dirnames, filenames in os.walk(self.path):
            if playlist_id in filenames:
                folder = root
                continue
        if not folder:
            return None
        m = re.match('^{}/?(.*)/?$'.format(self.path), folder)
        if m:
            return m.groups()[0]
        return None
