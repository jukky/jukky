
import json
from copy import deepcopy

DEFAULTS = {
    'library_path': '/opt/music/',  # Path to the music folder
    'log_file': '/opt/jukky.log',  # Logfile
    'interaction': {  # GPIO pins of buttons
        'shutdown': 7,
        'playpause': 11,
        'next': 15,
        'previous': 13,
        'vol_up': 16,
        'vol_down': 18,
    },
    'low_battery': 10,  # GPIO pin for low battery check
    'max_volume': 60,  # Max allowed volume
    'start_volume': 45,  # Max starting volume on startup
    'delta_volume': 5,  # In-/decrement volume with X percent
    'inactivity_shutdown': 5,  # Inactivity in minutes, after which the jukky shuts down
    'led_support': True,  # Flag for LED usage
    'led_pins': {  # GPIO pins of LEDs, if no RGB-LED present, just use one of the following
        'red': 29,
        'blue': 31,
        'green': None
    },
}


def load_config(file_path):
    with open(file_path, 'r') as f:
        config = json.load(f)
    merged = deepcopy(DEFAULTS)
    merged.update(config)
    return merged
