import re

import sh


class MpcClient(object):
    """Client to communicate with the mpc cli tool."""

    VOLUME_PATTERN = r'^volume:\W{0,2}([0-9]{1,3})%$'
    COMMAND = 'mpc'

    def __init__(self, library_directory):
        self.client = sh.Command(self.COMMAND).bake(h='localhost')
        self.library_dir = library_directory

    @property
    def volume(self):
        ret = self.client.volume().rstrip()
        match = re.match(self.VOLUME_PATTERN, ret)
        if not match:
            return None
        return int(match.groups()[0])

    @volume.setter
    def volume(self, volume):
        self.client.volume(volume)

    def play(self):
        self.client.play()

    def pause(self):
        self.client.pause()

    def switch_play_pause(self):
        self.client.toggle()

    def next(self):
        self.client.next()

    def previous(self):
        self.client.previous()

    def clear_playlist(self):
        self.client.clear()

    def add_song_to_playlist(self, song):
        self.client.add(song.rstrip())

    def play_directory(self, directory):
        self.clear_playlist()
        for song in self.get_songs_from_directory(directory):
            self.add_song_to_playlist(song)
        self.play()

    def get_songs_from_directory(self, directory):
        return self.client.ls(directory)

    def is_playing(self):
        return '[playing]' in self.client.status()
