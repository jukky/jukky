import logging
from collections import namedtuple
from datetime import datetime
from multiprocessing import Pipe, Process

from actions import handle_action, ACTION_TYPE_RFID, ACTION_TYPE_BUTTON, ACTION_TYPE_STATUS
from configuration import load_config
from library import Library
from players.mpc import MpcClient
from processes import ButtonWatchProcess, RFIDReaderProcess, LowBatteryWatchProcess, InactivityWatchProcess


def run_process(target_class, connection, application, *args, **kwargs):
    pcs = target_class(connection, application)
    pcs.run(*args, **kwargs)


ProcessConfiguration = namedtuple('ProcessConfiguration', ['name', 'process_class', 'action_type'])

logger = logging.getLogger('jukky')


class Application(object):
    _running_processes = {}
    _processes = (
        ProcessConfiguration(name='RFID reader', process_class=RFIDReaderProcess, action_type=ACTION_TYPE_RFID),
        ProcessConfiguration(name='Button watcher', process_class=ButtonWatchProcess, action_type=ACTION_TYPE_BUTTON),
        ProcessConfiguration(name='Low battery watcher', process_class=LowBatteryWatchProcess,
                             action_type=ACTION_TYPE_STATUS),
        ProcessConfiguration(name='Inactivity watcher', process_class=InactivityWatchProcess,
                             action_type=ACTION_TYPE_STATUS),
    )

    def __init__(self, config):
        """
        :param dict config: Configuration object
        """
        self.config = config
        self.configure_logging(self.config.get('log_file'))
        self.library = Library(config.get('library_path'))
        self.player = self.initialise_player()
        self.last_activity = datetime.now()

    def configure_logging(self, log_file):
        logger = logging.getLogger('jukky')
        logger.setLevel(logging.INFO)
        fh = logging.FileHandler(log_file)
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s %(name)s(%(levelname)s): %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        logger.addHandler(fh)
        logger.addHandler(ch)

    def initialise_player(self):
        """
        Create a player instance and configure it.
        :return: MpcClient
        """
        player = MpcClient(library_directory=self.library)
        player.volume = self.config.get('start_volume', 40)
        return player

    def run(self):
        self.start_processes()
        self.start_led()
        self.wait()

    def start_processes(self):
        for conf in self._processes:
            read_con, w_c = Pipe(duplex=False)
            kwargs = dict(connection=w_c, target_class=conf.process_class, application=self)
            process = Process(name=conf.name, target=run_process, kwargs=kwargs)
            self._running_processes[read_con] = conf
            process.daemon = True
            process.start()
            w_c.close()
            logger.info('Started process {}'.format(conf.name))

    def start_led(self):
        from processes.helper import get_gpio_module
        G = get_gpio_module()
        G.setmode(G.BOARD)
        G.setup([29, 31], G.OUT)
        G.output([29, 31], G.HIGH)

    def wait(self):
        while self._running_processes:
            for con, process_conf in self._running_processes.items():
                if con.poll():
                    try:
                        msg = con.recv()
                    except EOFError:
                        logger.info('Process {} stopped!'.format(process_conf.name))
                        del self._running_processes[con]
                    else:
                        self.last_activity = datetime.now()
                        handle_action(process_conf.action_type, msg, library=self.library, player=self.player,
                                      config=self.config)


if __name__ == '__main__':
    config = load_config('../local.conf')
    app = Application(config)
    app.run()
