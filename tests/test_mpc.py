import os
from unittest.mock import patch, Mock

import pytest
from sh import RunningCommand

from jukky.players.mpc import MpcClient


@pytest.fixture
def mock_os(monkeypatch):
    monkeypatch.setattr(os.path, 'exists', Mock(return_value=True))
    monkeypatch.setattr(os.path, 'isfile', Mock(return_value=True))
    monkeypatch.setattr(os, 'access', Mock(return_value=True))


@pytest.fixture
def mpc_client(mock_os):
    with patch.object(MpcClient, 'COMMAND', '/usr/bin/dummy'):
        mpc_client = MpcClient('/tmp/')
    return mpc_client


class TestMpc:

    @pytest.mark.parametrize('method,expected_calls', (
        ['pause', ['pause']],
        ['play', ['play']],
        ['next', ['next']],
        ['previous', ['previous']],
        ['clear_playlist', ['clear']],
    ))
    @patch.object(RunningCommand, '__init__', return_value=None)
    def test_base_calls(self, init_mock, method, expected_calls, mpc_client):
        getattr(mpc_client, method)()
        init_mock.assert_called_once()
        for i, call in enumerate(init_mock.call_args_list):
            cmd = b' '.join(call[0][0])
            assert cmd == '/usr/bin/dummy -h localhost {}'.format(expected_calls[i]).encode()

    @patch.object(RunningCommand, '__init__', return_value=None)
    def test_set_volume(self, init_mock, mpc_client):
        mpc_client.volume = 20
        init_mock.assert_called_once()
        cmd = b' '.join(init_mock.call_args[0][0])
        assert cmd == b'/usr/bin/dummy -h localhost volume 20'

    @pytest.mark.parametrize('mpc_return_value,expected', (
        ['volume: 10%', 10],
        ['volume:100%', 100],
        ['volume:  1%', 1],
        ['volume: n/a', None],
    ))
    def test_get_volume(self, mpc_client, monkeypatch, mpc_return_value, expected):
        monkeypatch.setattr(mpc_client, 'client', Mock())
        mpc_client.client.volume = Mock(return_value=mpc_return_value)
        ret = mpc_client.volume
        assert ret == expected

    @pytest.mark.parametrize('song, expected', (
        ["That's my/Greatest And/1 best song.mp3", b'That\'s my/Greatest And/1 best song.mp3'],
        ["That's my/Greatest And/1 best song.mp3  \n ", b'That\'s my/Greatest And/1 best song.mp3'],
        ["That's my/Greatest And/1 best song.mp3 \n", b'That\'s my/Greatest And/1 best song.mp3'],
        ["That's my/Greatest And/1 best song.mp3  ", b'That\'s my/Greatest And/1 best song.mp3'],
    ))
    @patch.object(RunningCommand, '__init__', return_value=None)
    def test_add_song_to_playlist(self, init_mock, mpc_client, song, expected):
        mpc_client.add_song_to_playlist(song)
        init_mock.assert_called_once()
        cmd = b' '.join(init_mock.call_args[0][0])
        assert cmd == b'/usr/bin/dummy -h localhost add ' + expected

    def test_play_directory(self, mpc_client):
        songs = ['ham/n/eggs.mp3', 'eggs/n/ham.mp3']
        play_mock = Mock()
        add_mock = Mock()
        clear_mock = Mock()
        songs_from_dir_mock = Mock(return_value=songs)
        with patch.multiple(mpc_client, play=play_mock, clear_playlist=clear_mock, add_song_to_playlist=add_mock,
                            get_songs_from_directory=songs_from_dir_mock):
            mpc_client.play_directory('foobar')
        clear_mock.assert_called_once()
        songs_from_dir_mock.assert_called_once_with('foobar')
        for song in songs:
            add_mock.assert_any_call(song)
        play_mock.assert_called_once()

    @patch.object(RunningCommand, '__init__', return_value=None)
    def test_add_song_to_playlist(self, init_mock, mpc_client):
        mpc_client.get_songs_from_directory('foobar')
        init_mock.assert_called_once()
        cmd = b' '.join(init_mock.call_args[0][0])
        assert cmd == b'/usr/bin/dummy -h localhost ls foobar'
