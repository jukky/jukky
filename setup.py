#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import dirname, join
from setuptools import setup, find_packages


def read(*args):
    return open(join(dirname(__file__), *args)).read()


exec(read('jukky', 'version.py'))


classifiers = """\
# The next line is important: it prevents accidental upload to PyPI!
Private :: Do Not Upload
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
License :: MIT License
#Operating System :: Microsoft :: Windows
Operating System :: POSIX
#Operating System :: MacOS :: MacOS X
Programming Language :: Python
#Programming Language :: Python :: 3.5
Topic :: Internet
"""

install_requires = [
    'sh',
]

setup(
    name='Jukky',
    version=__version__,  # noqa
    description='A library for a raspberry pi based jukebox',
    long_description=read('README.md'),
    author='Omit Idle',
    author_email='timo@wumpitz.de',
    maintainer='Omit Idle',
    maintainer_email='timo@wumpitz.de',
    url='https://gitlab.com/jukky/jukky/',
    license="MIT License",
    classifiers=[c.strip() for c in classifiers.splitlines()
                 if c.strip() and not c.startswith('#')],
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    test_suite='tests',
    scripts=['jukky/bin/jukky'],
    install_requires=install_requires,
)
